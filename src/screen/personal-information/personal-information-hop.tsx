import React, { useEffect, useState } from 'react';
import {
  ChangeAvatarImage,
  Box,
  Queue,
  Spacing,
  Stack,
  Text,
  Button,
  Input,
  Banner,
  AnimatedView,
} from '../../components';
import { ScrollView } from 'react-native-gesture-handler';
import { useDocument } from '../../hooks';
import auth from '@react-native-firebase/auth';
import { GenderDashboard } from '../registration/gender-dashboard';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const PersonalInformationHop = () => {
  const [state, setState] = useState({
    lastName: 'lastName',
    firstName: 'firstName',
    age: 'age',
    edu: 'edu',
    grade: 'grade',
    number: 'number',
    number1: 'number1',
    mail: 'mail',
    gender: 'neutral',
  });
  const [start, setStart] = useState(false);
  const user: any = auth().currentUser;
  const userData: any = useDocument(`users/${user.uid}`);
  const insets = useSafeAreaInsets();

  useEffect(() => {
    if (userData.doc != null) {
      setState((state) => ({ ...state, lastName: userData.doc.lastname }));
      setState((state) => ({ ...state, firstName: userData.doc.firstname }));
      setState((state) => ({ ...state, age: userData.doc.age }));
      setState((state) => ({ ...state, edu: userData.doc.edu }));
      setState((state) => ({ ...state, firstName: userData.doc.grade }));
      setState((state) => ({ ...state, number: userData.doc.number }));
      setState((state) => ({ ...state, number1: userData.doc.number1 }));
      setState((state) => ({ ...state, mail: userData.doc.mail }));
      setState((state) => ({ ...state, gender: userData.doc.gender }));
    }
  }, [userData.doc]);

  return (
    <Box>
      <Box
        width="100%"
        height={64}
        position="absolute"
        top={-100}
        zIndex={1000}
        justifyContent="center"
        alignItems="center"
      >
        <AnimatedView width={342} height="100%" changeY={140} start={start}>
          <AnimatedView
            width="100%"
            height="100%"
            changeO={0}
            start={start}
            duration={4000}
          >
            <Banner type="success" title="Амжилттай хадаглагдлаа"></Banner>
          </AnimatedView>
        </AnimatedView>
      </Box>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: '#FAFAFA',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Spacing p={4}>
          <Spacing m={6}>
            <ChangeAvatarImage size="L" />
          </Spacing>
          <Stack size={3} alignItems={'center'}>
            <Input
              placeholder={'Овог'}
              value={state.lastName}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, lastName: text }))
              }
            ></Input>
            <Input
              placeholder={'Нэр'}
              value={state.firstName}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, firstName: text }))
              }
            ></Input>
            <Input
              placeholder={'Нас'}
              value={state.age}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, age: text }))
              }
            ></Input>
            <Input
              placeholder={'Боловсрол'}
              value={state.edu}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, edu: text }))
              }
            ></Input>
            <Input
              placeholder={'Анги'}
              value={state.grade}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, grade: text }))
              }
            ></Input>
            <Spacing mt={1}>
              <Queue size={2}>
                <GenderDashboard
                  desc={'Эрэгтэй'}
                  type={'male'}
                  isSelected={state.gender === 'male'}
                  onPress={() =>
                    setState((state) => ({ ...state, gender: 'male' }))
                  }
                />
                <GenderDashboard
                  desc={'Эмэгтэй'}
                  type={'female'}
                  isSelected={state.gender === 'female'}
                  onPress={() =>
                    setState((state) => ({ ...state, gender: 'female' }))
                  }
                />
                <GenderDashboard
                  desc={'Бусад'}
                  type={'neutral'}
                  isSelected={state.gender === 'neutral'}
                  onPress={() =>
                    setState((state) => ({ ...state, gender: 'neutral' }))
                  }
                />
              </Queue>
            </Spacing>
            <Spacing mt={3} mb={2}>
              <Box role={'primary200'} height={1} width={342} />
            </Spacing>
            <Spacing mb={2}>
              <Text
                type={'headline'}
                fontFamily={'Montserrat'}
                width={342}
                textAlign="left"
                bold
              >
                Холбогдох мэдээлэл
              </Text>
            </Spacing>
            <Input
              placeholder={'Утас'}
              value={state.number}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, number: text }))
              }
            ></Input>
            <Input
              placeholder={'Яааралтай үед холбоо барих'}
              value={state.number1}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, number1: text }))
              }
            ></Input>
            <Input
              placeholder={'И-мейл'}
              value={state.mail}
              onChangeText={(text: any) =>
                setState((state) => ({ ...state, mail: text }))
              }
            ></Input>
            <Spacing mt={3} mb={4 + insets.bottom / 4}>
              <Button
                size={'l'}
                width={342}
                type="primary"
                onPress={() => {
                  userData.updateRecord(state);
                  setStart(true);
                }}
              >
                <Text
                  role={'white'}
                  bold
                  fontFamily={'Montserrat'}
                  type={'callout'}
                >
                  Хадгалах
                </Text>
              </Button>
            </Spacing>
          </Stack>
        </Spacing>
      </ScrollView>
    </Box>
  );
};
