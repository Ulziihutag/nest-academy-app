import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const SignUpUser: React.FC<IconType> = ({ width = 56, height = 56 }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 56 56" fill="none">
      <Path
        d="M46.6673 49V44.3333C46.6673 41.858 45.684 39.484 43.9336 37.7337C42.1833 35.9833 39.8093 35 37.334 35H18.6673C16.192 35 13.818 35.9833 12.0677 37.7337C10.3173 39.484 9.33398 41.858 9.33398 44.3333V49"
        stroke="#111111"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <Path
        d="M27.9993 25.6667C33.154 25.6667 37.3327 21.488 37.3327 16.3333C37.3327 11.1787 33.154 7 27.9993 7C22.8447 7 18.666 11.1787 18.666 16.3333C18.666 21.488 22.8447 25.6667 27.9993 25.6667Z"
        stroke="#111111"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </Svg>
  );
};
