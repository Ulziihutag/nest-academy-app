import React, { useState, useContext } from 'react';
import { Image } from 'react-native';
import {
  AdmissionProcessCard,
  AdmissionProcessCardContent,
  AdmissionProcessCardHeader,
} from '../../components/admission-process-card';
import { ExamContext } from '../../provider/exam-provider';
import {
  Spacing,
  Box,
  BackgroundImage,
  Stack,
  Queue,
  Text,
  Lozenge,
  InputWithHelperTextAndCounter,
  CheckBoxItem,
  GroupCheckBox,
} from '../../components';
import _ from 'lodash';

export const ExamCard: React.FC<any> = (props) => {
  const { updateAnswer } = useContext(ExamContext);
  const ans = ['A', 'B', 'C', 'D', 'E', 'F'];
  const [inputText, setInputText] = useState('');
  const {
    id,
    qId = 1,
    type,
    questionPicture,
    question = 'Дөрвөн дүрсний аль гурав нь нийлж нэг дөрвөлжин үүсгэх вэ?',
    answerPicture,
    answers,
    userAnswer,
  } = props;

  console.log(type, answerPicture);
  return (
    <Box>
      <AdmissionProcessCard>
        <AdmissionProcessCardHeader>
          <Box role={userAnswer && 'primary200'} flexDirection={'column'}>
            <Spacing pv={3} ph={4}>
              <Spacing mb={4}>
                <Box flexDirection={'row'} justifyContent={'space-between'}>
                  <Text
                    fontFamily={'Montserrat'}
                    role={userAnswer ? 'gray' : 'gray'}
                    bold
                    type={'subheading'}
                  >
                    Асуулт №{qId}
                  </Text>
                  <Lozenge
                    style={'subtle'}
                    type={userAnswer ? 'default' : 'pending'}
                  >
                    {userAnswer ? 'Хариулсан' : 'Хариулаагүй'}
                  </Lozenge>
                </Box>
              </Spacing>
              <Text
                fontFamily={'Montserrat'}
                role={'primary500'}
                type={'body'}
                bold
              >
                {question}
              </Text>
            </Spacing>
          </Box>
          {questionPicture && (
            <Box alignItems={'center'}>
              <Image
                style={{ width: 311, height: 242, resizeMode: 'contain' }}
                source={{ uri: questionPicture }}
              />
            </Box>
          )}
        </AdmissionProcessCardHeader>
        <AdmissionProcessCardContent>
          <Spacing m={4}>
            {type == 'fill' ? (
              <Box justifyContent={'center'} alignItems={'center'}>
                <InputWithHelperTextAndCounter
                  placeholder={'Хариулт'}
                  helperText={'Та дээрх хэсэгт өөрийн хариултаа бичнэ үү.'}
                  onChangeText={(text: string) => setInputText(text)}
                  onSubmitEditing={() => updateAnswer(id, inputText)}
                />
              </Box>
            ) : (
              <Stack size={6}>
                <Box width={'100%'} height={'auto'}>
                  {answerPicture && (
                    <Box flexDirection={'column'}>
                      <BackgroundImage
                        width={'100%'}
                        source={{ uri: answerPicture }}
                        resizeMode={'contain'}
                        aspectRatio={1}
                      ></BackgroundImage>
                    </Box>
                  )}
                  <Stack size={2}>
                    {!answerPicture &&
                      answers.map((e: any, i: any) => {
                        return (
                          <Queue key={`${id}-answer${i}`} size={1}>
                            <Text type={'body'} bold>
                              {ans[i] + ') '}
                            </Text>
                            <Text type={'body'}>{e}</Text>
                          </Queue>
                        );
                      })}
                  </Stack>
                </Box>
                <Box
                  height={60}
                  flexDirection={'row'}
                  justifyContent={'space-between'}
                >
                  <GroupCheckBox size={6}>
                    {_.map(answers, (e, i: any) => {
                      return (
                        <CheckBoxItem
                          size={[12, 8, 12]}
                          checkbox
                          index={i}
                          onPress={() => {
                            updateAnswer(id, answers[i]);
                          }}
                          onUnPress={() => {}}
                        >
                          <Box width={56} height={56}>
                            <Spacing m={3}>
                              <Box
                                height="100%"
                                justifyContent="center"
                                alignItems="center"
                              >
                                <Text type={'callout'} bold>
                                  {ans[i]}
                                </Text>
                              </Box>
                            </Spacing>
                          </Box>
                        </CheckBoxItem>
                      );
                    })}
                  </GroupCheckBox>
                </Box>
              </Stack>
            )}
          </Spacing>
        </AdmissionProcessCardContent>
      </AdmissionProcessCard>
    </Box>
  );
};
