import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
  width?: string | number;
  height?: string | number;
  color?: string;
}

export const EyeIcon: React.FC<Props> = ({
  width = 22,
  height = 22,
  color = '#8F9CB3',
  ...others
}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 22 22"
      fill="none"
      {...others}
    >
      <Path
        d="M1.083 11C1.944 6.307 6.057 2.75 11 2.75s9.055 3.557 9.917 8.25c-.861 4.693-4.974 8.25-9.917 8.25S1.945 15.693 1.083 11zM11 15.583a4.583 4.583 0 100-9.166 4.583 4.583 0 000 9.166zm0-1.833a2.75 2.75 0 110-5.5 2.75 2.75 0 010 5.5z"
        fill={color}
      />
    </Svg>
  );
};
