export type ColorType =
  | 'white'
  | 'offwhite'
  | 'fawhite'
  | 'black'
  | 'black100'
  | 'gray'
  | 'lightgray'
  | 'darkgray'
  | 'accentNest'
  | 'caution200'
  | 'caution300'
  | 'caution400'
  | 'caution500'
  | 'caution600'
  | 'destructive200'
  | 'destructive300'
  | 'destructive400'
  | 'destructive500'
  | 'primary'
  | 'primary100'
  | 'primary200'
  | 'primary300'
  | 'primary400'
  | 'primary500'
  | 'success200'
  | 'success300'
  | 'success400'
  | 'success500'
  | 'gray12';

export type IconType = {
  role?: ColorType;
  width?: number;
  height?: number;
};
