import React from 'react';
import {
  Box,
  Warning,
  ExamRafikiIllustration,
  Spacing,
  TopNavigation,
  Text,
} from '../../components';
import { useNavigation } from '@react-navigation/native';

export const ExamWarningScreen1 = () => {
  const navigation = useNavigation();
  return (
    <Box flex={1} justifyContent="space-evenly" role="white">
      <Box alignSelf="center">
        <ExamRafikiIllustration />
      </Box>
      <Spacing mh={4}>
        <Warning
          onPress={() => {
            navigation.navigate('ExamWarning2');
          }}
          title="Санамж #1"
          buttonname="Дараах"
        >
          <Text role="primary500" type="body" textAlign="center" width={275}>
            Элсэлтийн онлайн шалгалтыг нэг хүн нэг л удаа өгөх боломжтой.
          </Text>
        </Warning>
      </Spacing>
    </Box>
  );
};
