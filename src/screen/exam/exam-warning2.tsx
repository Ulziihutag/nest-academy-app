import React from 'react';
import {
  Box,
  Warning,
  TimeLineIllustration,
  TopNavigation,
  Spacing,
  Text,
} from '../../components';
import { useNavigation } from '@react-navigation/native';

export const ExamWarningScreen2 = () => {
  const navigation = useNavigation();
  return (
    <Box flex={1} justifyContent="space-evenly" role="white">
      <Box alignSelf="center">
        <TimeLineIllustration />
      </Box>
      <Spacing mh={4}>
        <Warning
          onPress={() => {
            navigation.navigate('ExamWarning3');
          }}
          title="Санамж #2"
          buttonname="Дараах"
        >
          <Text role="primary500" type="body" textAlign="center" width={275}>
            Шалгалт 30 асуултаас бүрдэх ба 30 минутад хийж гүйцэтгэнэ.
          </Text>
        </Warning>
      </Spacing>
    </Box>
  );
};
