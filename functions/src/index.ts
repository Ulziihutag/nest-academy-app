import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();
const db = admin.firestore();

export const sendNotificationToUser = async ({
    uid,
    title,
    body,
}: {
    uid: string;
    title: string;
    body?: string;
}) => {
    const user = await db.doc(`users/${uid}`).get();
    const data = user.data();
    const { notificationToken } = data || {};
    
    const message = {
        notification: {
            title: title || 'Nest Academy',
            body: body || '',
        },
        token: notificationToken,
    };

    console.log('sending message to ', JSON.stringify({ message, uid }));
    await admin.messaging().send(message);
}; 

export const onOrderHistoryUpdate = functions.firestore
    .document('users/{uid}')
    .onCreate(async (change, context) => {
        const { uid } = context.params;
        const { firstname } = change.data();

        await sendNotificationToUser({
            uid,
            title: `Welcome to Nest Academy App`,
            body: `Nest Academy-д бүртгүүлсэнд баярлалаа. ${firstname}`
        });
        return true;
    });