import React, { Children } from 'react';
import {
  Box,
  Text,
  CloseLineIcon,
  LeftArrowIcon,
  ProgressBar,
  Spacing,
} from '../components';
import { TouchableOpacity } from 'react-native';
import { navigate } from '@react-navigation/routers/lib/typescript/src/CommonActions';
import { useNavigation } from '@react-navigation/native';

type Header = {
  exit?: boolean;
  goBack?: boolean;
  timer?: boolean;
  exitTimer?: boolean;
  title?: string;
};

export const NavHeader: React.FC<Header> = ({
  exit,
  goBack,
  timer,
  exitTimer,
  title,
}) => {
  const navigation = useNavigation();
  return (
    <Box flex={1} flexDirection={'column'} alignItems={'center'}>
      <Box
        flex={2.5}
        flexDirection={'row'}
        alignItems={'center'}
        justifyContent={'center'}
        width={'95%'}
      >
        <Box width={'25%'} alignItems={'center'}>
          <Spacing mt={1.25}>
            {exitTimer === true && (
              <Box flex={1} flexDirection={'row'}>
                <Box>
                  <Spacing ml={10}>
                    <TouchableOpacity>
                      <LeftArrowIcon height={27} width={27} />
                    </TouchableOpacity>
                  </Spacing>
                </Box>
                <Box>
                  <Spacing ml={2}>
                    <Text type={'body'} bold role={'primary500'}>
                      29:59
                    </Text>
                  </Spacing>
                </Box>
              </Box>
            )}
            {goBack === true && (
              <Box flex={1} role={'caution600'} width={30} height={30}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <LeftArrowIcon height={27} width={27} />
                </TouchableOpacity>
              </Box>
            )}
            {timer === true && (
              <Box flex={1}>
                <Text type={'body'} bold role={'primary500'}>
                  29:59
                </Text>
              </Box>
            )}
          </Spacing>
        </Box>
        <Box width={'60%'} alignItems={'center'} justifyContent={'center'}>
          <Text bold type={'headline'} width={'100%'}>
            {title}
          </Text>
        </Box>
        <Box width={'25%'} alignItems="center">
          <Spacing mt={1}>
            {exit === true && (
              <TouchableOpacity>
                <CloseLineIcon height={23} width={23} />
              </TouchableOpacity>
            )}
          </Spacing>
        </Box>
      </Box>
      <Box flex={1} width={'100%'}>
        <ProgressBar
          backgroundRole="offwhite"
          role="success400"
          endProgress={20}
          width={'90%'}
          height={150}
        />
      </Box>
    </Box>
  );
};
