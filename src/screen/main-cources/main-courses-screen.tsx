import React, { createContext, useRef, useContext, useState } from 'react';
import { TouchableOpacity, FlatList, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { PhotoCard } from './photo-card';
import {
  Text,
  Spacing,
  Box,
  Stack,
  Border,
  Button,
  ExpandableText,
  TopBarNavigator,
  TopBarScreen,
  LoadingCircle,
  BackgroundImage,
  Toggle,
  TimeSchedule,
  Divider,
  Comment,
} from '../../components';
import { PrimaryRightArrowIcon } from '../../components/icons'
import { AdmissionProcess } from './admission-process';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useQuery } from '@apollo/client';
import { REQUEST_MAIN_DETAIL_SCREEN } from '../../graphql/queries';
import _ from 'lodash';
import { AdvantagesCard } from './advantages-card';
import { CurriculmCard } from './curriculum-card';

export const MainCourseContext = createContext({
  data: [],
  loading: true,
  courseLogo: '',
  advantages: [],
  courseDescription: '',
  timeTables: [],
  teachersInformation: [],
  admissionProcess: [],
  curriculumData: [],
  demoProjects: [],
});

export const MainCourse: React.FC<any> = ({ route }) => {
  const navigation = useNavigation();
  const { loading, courseLogo } = useContext(MainCourseContext);

  if (loading) {
    return (
      <Box flex={1} alignItems={'center'} justifyContent={'center'}>
        <LoadingCircle />
      </Box>
    );
  }

  const MainCoursePoster = () => {
    return (
      <BackgroundImage resizeMode={'cover'} width={'100%'} height={280} source={{ uri: courseLogo }} />
    );
  };

  return (
    <Box>
      <TopBarNavigator Header={MainCoursePoster}>
        <TopBarScreen name="Тухай" component={MainCourseAboutScreen} />
        <TopBarScreen name="Хөтөлбөр" component={ProgramScreen} />
        <TopBarScreen name="Бүтээлүүд" component={ProjectScreen} />
      </TopBarNavigator>
      <Box bottom={0} position={'absolute'} width={'100%'} height={90} role={'white'}>
        <Spacing pt={1} pl={4} pr={4}>
          <Button
            size={'l'}
            width={'100%'}
            onPress={() => navigation.navigate(NavigationRoutes.Admission)}
          >
            <Text fontFamily={'Montserrat'} type={'callout'} role={'white'} bold>
              БҮРТГҮҮЛЭХ
          </Text>
          </Button>
        </Spacing>
      </Box>
    </Box>
  );
};


export const MainCourseAboutScreen = () => {
  const navigation = useNavigation();
  const { data, loading, advantages, courseDescription, timeTables, teachersInformation } = useContext(MainCourseContext);
  const teachers = teachersInformation.map((teacher: any, index: Number) => ({ ...teacher, index: index }));

  const [index, setIndex] = useState(0);

  const onViewRef = useRef((viewableItems: any) => {

    if (viewableItems.viewableItems.length > 0) {
      let currentIndex = viewableItems.viewableItems[0].index;
      setIndex(currentIndex);
    }

  });
  const viewConfigRef = useRef({
    viewAreaCoveragePercentThreshold: 50,
  });


  if (loading) {
    return (
      <Box flex={1} alignItems={'center'} justifyContent={'center'}>
        <LoadingCircle />
      </Box>
    );
  }

  return (
    <Box flex={1}>
      <Spacing mh={4} pt={9}>
        <Stack size={6}>
          <Box>
            <Text fontFamily={'Montserrat'} bold type={'body'}>
              Давуу талууд
          </Text>
            <Spacing mt={8}>
              <Stack size={6}>
                <FlatList
                  data={advantages}
                  renderItem={({ item }: any) => {
                    return <AdvantagesCard
                      icon={item.icon.url}
                      title={item.title}
                      description={item.description}
                    />
                  }}
                  keyExtractor={(item: any, index) => item.sys ? item.sys.id : `advantages-${index}`}
                  ItemSeparatorComponent={() => <Box height={24} />}
                />
              </Stack>
            </Spacing>
          </Box>
          <Divider />
          <ExpandableText text={courseDescription} />
          <Divider />
          <Box width={'100%'}>
            <Text fontFamily={'Montserrat'} bold type={'body'}>
              Цаг хувиарлалт
          </Text>
            <Spacing mt={4}>
              <Box height={'auto'} width={'100%'}>
                <Toggle sessions={timeTables.map((cur: any) => cur.session)}>
                  {
                    _.map(timeTables, (item: any, index) => <Stack width={'100%'} key={item.sys.id} size={6}>
                      <Border radius={8}>
                        <Box width={'100%'} role={'caution300'} >
                          <Spacing p={3}>
                            <Text role={'black'}>{item.description.json.content[0].content[0].value}</Text>
                          </Spacing>
                        </Box>
                      </Border>
                      <Stack size={2}>
                        {_.map(item.timeBlocks, ({ title, body, startDate, endData }) => <TimeSchedule title={title} from={endData} to={startDate}>{body}</TimeSchedule>)}
                      </Stack>
                    </Stack>
                    )
                  }
                </Toggle>
              </Box>
            </Spacing>
          </Box>
          <Divider />
          <Box width={'100%'}>
            <Spacing mb={4}>
              <Text fontFamily={'Montserrat'} bold type={'body'}>
                Багш нар
            </Text>
            </Spacing>
            <FlatList
              data={teachers}
              viewabilityConfig={viewConfigRef.current}
              onViewableItemsChanged={onViewRef.current}
              pagingEnabled={true}
              style={{ overflow: 'visible' }}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item, index }) => {
                return (
                  <PhotoCard
                    key={item.index}
                    ratio={0.9}
                    source={{ uri: item.image.url }}
                    title={item.class}
                    name={item.name}
                    width={Dimensions.get('window').width - 52}
                  />
                );
              }}
              keyExtractor={(item, index) => item.sys ? item.sys.id : `teacher-${index}`}
              ItemSeparatorComponent={() => <Box width={20} />}
              horizontal
            />
            <Spacing mt={6} />
            <Text bold type={'body'}>
              Дэлгэрэнгүй
          </Text>
            <Spacing mt={2} />
            <Text numberOfLines={4}>
              {teachers[index].introduction}
            </Text>
          </Box>
          <Divider />
          <Box>
            <Text fontFamily={'Montserrat'} bold type={'body'}>
              Элсэлтийн явц
          </Text>
            <Spacing m={4} />
            <AdmissionProcess />
          </Box>
        </Stack>
      </Spacing>
      <Spacing mv={6}>
        <Divider />
        <Box height={56} justifyContent="center">
          <Spacing mh={4}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate(NavigationRoutes.FaqScreen)
              }
            >
              <Box height={64} flexDirection={'row'} alignItems={'center'} justifyContent="space-between">
                <Text type="body" bold>
                  FAQ
                </Text>
                <PrimaryRightArrowIcon width={24} height={24} />
              </Box>
            </TouchableOpacity>
          </Spacing>
        </Box>
        <Divider />
      </Spacing>
      <Spacing m={15} />
    </Box>
  );
};

export const ProgramScreen = () => {
  const { curriculumData } = useContext(MainCourseContext);

  return (
    <Spacing mh={4} mt={4}>
      <Box width={'100%'} flex={1} alignItems={'center'}>
        <Box width={'100%'} height={'auto'}>
          <Toggle sessions={curriculumData.map((cur: any) => cur.name == 'design' ? 'Дизайн' : 'Программ')}>
            {
              curriculumData.map((cur: any) => <FlatList
                data={cur.curriculum}
                renderItem={(data: any) => {
                  return <CurriculmCard
                    level={data.item.level}
                    lessons={data.item.topics}
                    duration={'8 сар'}
                    number={'43 хичээл'}
                  />
                }}
                keyExtractor={(_, index) => `curriculum-${cur.sys.id}-${index}`}
                ItemSeparatorComponent={() => <Box width={16} />}
              />)
            }
          </Toggle>
        </Box>
      </Box>
    </Spacing>
  );
};

const CommentData = [
  {
    id: 1,
    source: require('../../assets/images/zedude.png'),
    name: 'Н. Цэрэнлхагва',
    title: '3-р түвшний сурагч',
    description:
      'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
  },
  {
    id: 2,
    source: require('../../assets/images/zedude.png'),
    name: 'Н. Цэрэнлхагва',
    title: '3-р түвшний сурагч',
    description:
      'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
  },
];

export const ProjectScreen = () => {
  const { demoProjects } = useContext(MainCourseContext);
  // const { data } = useQuery(REQUEST_DemoProject);
  const [height, setHeight] = useState(190)
  const navigation = useNavigation();
  const { width } = Dimensions.get('window');

  return (
      <Box>
        <Spacing mb={3} mt={5} ml={3}>
          <Text type="headline" role="primary500" bold textAlign="left">
            Demo Day 2020
          </Text>
        </Spacing>
        <Spacing mh={2}>
          <Box width={width - 16} justifyContent='flex-start' flexDirection='row' flexWrap='wrap' height={'auto'} overflow='hidden'>
            {demoProjects.map((d: any) => {
              return (
                <Spacing key={d.sys.id} m={1}>
                  <Border radius={4}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate(
                          NavigationRoutes.DemoDetails,
                          d
                        )
                      }
                    >
                      <BackgroundImage source={d.posterImage} width={width / 2 - 16} height={width / 2 - 16} />
                    </TouchableOpacity>
                  </Border>
                </Spacing>
              )
            })}
          </Box>
        </Spacing>
        <Spacing mt={8} mh={4}>
          <Text type="title3" role="primary500" bold>
            Сурагцын сэтгэгдэл
          </Text>
        </Spacing>
        <Spacing mh={4} mt={6}>
          <FlatList
            data={CommentData}
            horizontal
            keyExtractor={(_, index) => 'comment'+index}
            renderItem={({ item }) => (
              <Comment
                name={item.name}
                source={item.source}
                title={item.title}
                description={item.description}
                width={311}
                height={288}
              />
            )}
          />
        </Spacing>
      </Box>
  );
};

export const MainCourseScreen: React.FC<any> = ({ route }) => {
  const { entryId } = route.params || {};
  const { data, error, loading } = useQuery(REQUEST_MAIN_DETAIL_SCREEN, {
    variables: { entryId },
  });

  const courseLogo = _(data).get('mainCourse.courseLogo.url');
  const advantages = _(data).get('mainCourse.advantagesCollection.items');
  const courseDescriptionContent = _(data).get('mainCourse.courseDescription.json.content');
  const courseDescription = _.isArray(courseDescriptionContent) ? _(courseDescriptionContent).map((item: any) => _.get(_.head(item.content), 'value')).join('\n') : '';
  const teachersInformation = _(data).get('mainCourse.staffCollection.items');
  const timeTables = _(data).get('mainCourse.timetableCollection.items');
  const admissionProcess = _(data).get('mainCourse.admissionProcess');
  const curriculumData = _(data).get('mainCourse.curriculumCollection.items');
  const demoProjects = _(data).get('mainCourse.demoProjectsCollection.items');

  return <MainCourseContext.Provider
    value={{
      demoProjects,
      curriculumData,
      admissionProcess,
      teachersInformation,
      courseLogo,
      advantages,
      courseDescription,
      timeTables,
      data,
      loading
    }}>
    <MainCourse />
  </MainCourseContext.Provider>
}
